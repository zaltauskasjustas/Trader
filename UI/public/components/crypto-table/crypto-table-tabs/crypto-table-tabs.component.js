import template from './crypto-table-tabs.html';

const CryptoTableTabs = {
  template,
  bindings: {
    selectedTab: '<',
    onUpdate: '&',
  },
  controller: class CryptoTableTabs {
    $onInit() {
      this.selectTab = CryptoTableTabs.selectTab;
    }

    static selectTab(selectedTab) {
      this.onUpdate({
        $event: {
          selectedTab,
        },
      });
    }
  },
};

export default CryptoTableTabs;
