export default function ($http) {
  this.scanCoins = (exchange, interval) => $http.post(
    `/${exchange}/scan`,
    {
      filter: {
        interval,
      },
    },
  )
    .then(response => response.data)
    .catch(() => {
      throw Error(`Bad request: scanCoins(${exchange}, ${interval})`);
    });
}
