import template from './crypto-table-perfomance.html';
import style from '../crypto-table-overview/crypto-table-overview.scss';

const CryptoTablePerfomanceComponent = {
  template,
  bindings: {
    search: '=',
    assets: '<',
  },
  controller: class CryptoTablePerfomanceController {
    constructor(cryptoTableService) {
      'ngInject';

      this.cryptoTableService = cryptoTableService;
    }

    $onInit() {
      this.sortType = 'ticker';
      this.sortReverse = false;
    }

    onSortClick(name) {
      this.sortType = name;
      this.sortReverse = !this.sortReverse;
    }
  },
};

export default CryptoTablePerfomanceComponent;
