import angular from 'angular';
import CryptoTableComponent from './crypto-table.component';
import CryptoTableService from './crypto-table.service';
import CryptoTableOverviewComponent from './crypto-table-overview/crypto-table-overview.component';
import CryptoTablePerfomanceComponent from './crypto-table-perfomance/crypto-table-perfomance.component';
import CryptoTableTabsComponent from './crypto-table-tabs/crypto-table-tabs.component';
import SearchBarComponent from '../search-bar/search-bar.component';

export default angular.module('crypto-table', [
])
  .service('cryptoTableService', CryptoTableService)
  .component('cryptoTable', CryptoTableComponent)
  .component('cryptoTableOverview', CryptoTableOverviewComponent)
  .component('cryptoTablePerfomance', CryptoTablePerfomanceComponent)
  .component('cryptoTableTabs', CryptoTableTabsComponent)
  .component('searchBar', SearchBarComponent)
  .name;
