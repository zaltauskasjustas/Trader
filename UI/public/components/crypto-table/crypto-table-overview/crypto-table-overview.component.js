import template from './crypto-table-overview.html';
import style from './crypto-table-overview.scss';

const CryptoTableOverviewComponent = {
  template,
  bindings: {
    search: '=',
    assets: '<',
  },
  controller: class CryptoTableOverviewController {
    constructor(cryptoTableService) {
      'ngInject';

      this.cryptoTableService = cryptoTableService;
    }

    $onInit() {
      this.sortType = 'ticker';
      this.sortReverse = false;
    }

    onSortClick(name) {
      this.sortType = name;
      this.sortReverse = !this.sortReverse;
    }
  },
};

export default CryptoTableOverviewComponent;
