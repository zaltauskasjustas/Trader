import template from './crypto-table.html';
import style from './crypto-table.scss';

const CryptoTableComponent = {
  template,
  controller: class CryptoTableController {
    constructor($http, cryptoTableService) {
      this.$http = $http;
      this.cryptoTableService = cryptoTableService;
    }

    $onInit() {
      this.cryptoTableService.scanCoins('Kraken', '1d').then((assets) => {
        this.assets = assets;
      });
      this.searchText = '';
      this.selectedTab = 'overview';
    }

    updateSearch({ searchText }) {
      this.searchText = searchText;
    }

    selectTab({ selectedTab }) {
      this.selectedTab = selectedTab;
    }
  },
};

export default CryptoTableComponent;
