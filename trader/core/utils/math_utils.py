from toolz import curry


@curry
def mul(a, b):
    return a * b
